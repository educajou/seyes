// Tableau des noms des jours avec "Lo" ajouté en occitan et sans majuscule
const nomJours = [
  { fr: "Lundi", oc: "Lo diluns" },
  { fr: "Mardi", oc: "Lo dimars" },
  { fr: "Mercredi", oc: "Lo dimècres" },
  { fr: "Jeudi", oc: "Lo dijòus" },
  { fr: "Vendredi", oc: "Lo divendres" },
  { fr: "Samedi", oc: "Lo dissabte" },
  { fr: "Dimanche", oc: "Lo dimenge" }
];
  
const nomMois = [
  { fr: "janvier", oc: "de genièr" },
  { fr: "février", oc: "de febrièr" },
  { fr: "mars", oc: "de març" },
  { fr: "avril", oc: "d'abril" },
  { fr: "mai", oc: "de mai" },
  { fr: "juin", oc: "de junh" },
  { fr: "juillet", oc: "de julhet" },
  { fr: "août", oc: "d'agost" },
  { fr: "septembre", oc: "de setembre" },
  { fr: "octobre", oc: "d'octobre" },
  { fr: "novembre", oc: "de novembre" },
  { fr: "décembre", oc: "de decembre" }
];
  