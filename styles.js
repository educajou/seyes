
// Assurez-vous que Rangy est initialisé
rangy.init();

// Crée un ClassApplier pour la classe "souligne"
const souligneClassApplier = rangy.createClassApplier("souligne", {
    elementTagName: "span", // Utilise un span pour appliquer la classe
    normalize: true // Fusionne les nœuds adjacents si possible
});

// Fonction pour ajouter/supprimer le soulignement
function souligne(couleur) {
    console.log('-----------------')
    console.log('function souligne',couleur)
    console.log('-----------------')

    // Mettre à jour l'aperçu de la couleur (si applicable)
    const previewElement = document.getElementById('souligne');
    if (previewElement && couleur) {
        previewElement.style.textDecorationColor = couleur;
        previewElement.value = couleur;
    }

    // Récupérer la sélection active
    const selection = rangy.getSelection();
    if (!selection || selection.isCollapsed) {
        alert("Veuillez sélectionner un texte."); // Empêche toute action si rien n'est sélectionné
        return;
    }

    console.log(texte_principal.innerHTML)


    // Supprimer le soulignement existant, quelle que soit la couleur    
    souligneClassApplier.undoToSelection();
    removeColorClasses();

    console.log(texte_principal.innerHTML)

    // Si la couleur est différente de "white", appliquer le soulignement et la couleur
    if (couleur !== "white") {
        souligneClassApplier.applyToSelection(); // Applique la classe "souligne"
        applyColorClass(couleur); // Ajoute la classe de couleur
    }

    console.log(texte_principal.innerHTML)


    
}

// Supprime toutes les classes de couleur existantes
function removeColorClasses() {
    console.log('function removeColorClasses')
    const selection = rangy.getSelection();
    if (!selection.rangeCount) return;

    const range = selection.getRangeAt(0); // Récupère la plage actuelle
    const nodes = range.getNodes([1], node => node.classList?.contains("souligne")); // Récupère les nœuds "souligne"

    nodes.forEach(node => {
        node.classList.remove(
            "red", "green", "orange", "black", "pink", "purple", "blue"
        ); // Supprime explicitement toutes les couleurs possibles
    });
}

// Ajoute une classe de couleur à la sélection
function applyColorClass(couleur) {
    console.log('function applyColorClass',couleur)

    const selection = rangy.getSelection();
    console.log('selection',selection);
    if (!selection.rangeCount) return;

    const range = selection.getRangeAt(0); // Récupère la plage actuelle
    console.log(range.getNodes())
    const nodes = range.getNodes([1], node => node.classList?.contains("souligne")); // Récupère les nœuds "souligne"
    console.log('nodes',nodes);
    nodes.forEach(node => {
        node.classList.add(couleur); // Ajoute la classe correspondant à la couleur
    });
}


// Crée un ClassApplier pour la classe "entoure"
const entoureClassApplier = rangy.createClassApplier("entoure", {
    elementTagName: "span", // Utilise <span> pour entourer le texte
    normalize: true // Fusionne les nœuds adjacents si possible
});

// Fonction pour appliquer ou supprimer la classe .entoure
function entoure() {
    entoureClassApplier.toggleSelection();
}


// Fonction pour appliquer la couleur au texte sélectionné ou au texte en cours de frappe
function couleurs(couleur) {
    console.log('------------------');
    console.log('couleurs', couleur);

    // Récupérer la sélection actuelle
    var selection = window.getSelection();

    if (selection.rangeCount > 0) {
        try {
            // Obtenir le range de la sélection
            var range = selection.getRangeAt(0);

            // Créer un fragment de document pour contenir la sélection
            var docFragment = range.cloneContents();

            // Fonction récursive pour appliquer la couleur au texte et aux éléments enfants
            function appliquerCouleurAuFragment(fragment) {
                var nodes = fragment.childNodes;
                for (var i = 0; i < nodes.length; i++) {
                    var node = nodes[i];
                    
                    if (node.nodeType === 3) {
                        // Si le nœud est du texte, on applique la couleur
                        var span = document.createElement('span');
                        if (couleur === 'transparent') {
                            span.classList.add('transparenttext')
                        }
                        span.style.color = couleur;
                        span.textContent = node.textContent;
                        node.replaceWith(span); // Remplace le nœud texte par un span
                    } else if (node.nodeType === 1) {
                        // Si le nœud est un élément (par exemple, un span ou un div), appliquer la couleur à l'élément
                        if (!node.style.color) { // Si l'élément n'a pas encore de couleur appliquée
                            node.style.color = couleur;
                        }
                        // Si l'élément contient des enfants, on applique récursivement la couleur
                        if (node.hasChildNodes()) {
                            appliquerCouleurAuFragment(node);
                        }
                    }
                }
            }

            // Appliquer la couleur au fragment de la sélection
            appliquerCouleurAuFragment(docFragment);

            // Remplacer le contenu original par le fragment modifié
            range.deleteContents();
            range.insertNode(docFragment);

        } catch (error) {
            console.error("Erreur lors de la modification de la sélection :", error);
        }
    } else {
        console.log("Aucune sélection trouvée.");
    }

    // Mettre à jour la couleur du bouton
    couleurTexte.style.color = couleur; // Applique la couleur au texte du bouton
    couleurTexte.value = couleur; // Met à jour la valeur du bouton si c'est un input
    if (couleur === 'transparent') {
        // Si la couleur est transparente, appliquer un effet de bordure nette sur le texte du bouton
        couleurTexte.style.color = '#ffffff';
        couleurTexte.style.textShadow = '1px 1px 0 rgba(0, 0, 0, 0.7), -1px -1px 0 rgba(0, 0, 0, 0.7), 1px -1px 0 rgba(0, 0, 0, 0.7), -1px 1px 0 rgba(0, 0, 0, 0.7)';
    } else {
        // Si la couleur n'est pas transparente, réinitialiser le textShadow pour enlever l'effet de bordure
        couleurTexte.style.textShadow = 'none';
    }
}


