(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.fr = {}));
}(this, (function (exports) { 'use strict';

  var fp = typeof window !== "undefined" && window.flatpickr !== undefined
      ? window.flatpickr
      : {
          l10ns: {},
      };
  var Occitan = {
      firstDayOfWeek: 1,
      weekdays: {
          shorthand: ["dim", "dl", "dma", "dme", "dj", "dv", "ds"],
          longhand: [
              "dimenge",
              "diluns",
              "dimars",
              "dimècres",
              "dijòus",
              "divendres",
              "dissabte",
          ],
      },
      months: {
          shorthand: [
              "gen",
              "febr",
              "març",
              "abr",
              "mai",
              "jun",
              "jul",
              "ag",
              "set",
              "oct",
              "nov",
              "dec",
          ],
          longhand: [
              "genièr",
              "febrièr",
              "març",
              "abril",
              "mai",
              "junh",
              "julhet",
              "agost",
              "setembre",
              "octobre",
              "novembre",
              "decembre",
          ],
      },
      ordinal: function (nth) {
          if (nth > 1)
              return "";
          return "èr";
      },
      rangeSeparator: " a ",
      weekAbbreviation: "Sem",
      scrollTitle: "Desfilar per aumentar",
      toggleTitle: "Clicar per bascular",
      time_24hr: true,
  };
  fp.l10ns.oc = Occitan;
  var oc = fp.l10ns;

  exports.Occitan = Occitan;
  exports.default = oc;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
