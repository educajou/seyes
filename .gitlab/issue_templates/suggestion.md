<!-- Avant de poster un ticket, vérifiez qu'un ticket similaire ne concerne pas déjà le même sujet.
Si c'est le cas, ajoutez plutôt votre message à sa suite pour donner des précisions. -->

<!-- Les notifications aux réponses seront reçues sur votre boîte mail académique. -->


## Description de la fonctionnalité demandée
<!-- Détaillez le fonctionnalité demandée. -->


## Objectif
<!-- À quoi ou dans quel contexte cela pourrait-il servir ? -->

## Mise en oeuvre
<!-- Un bouton ? Un panneau ? À quel endroit de la page ? -->


## Informations supplémentaires
<!-- Ajoutez si nécessaire des captures d'écran, un dessin, ou tout autre chose.-->
